# Plugins I like (disable if they will not be used)
plugins=(
  cp
  git
  django
  docker
  docker-machine
  docker-compose
  history
  ng
  node
  npm
  pip
  python
  sudo
  systemd
)


### Useful aliases / settings

## Navigation/listing

alias .2='cd ../../'
alias .3='cd ../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../../'

# List all except (./..) with relative size units and some others parameters
alias la='ls -Alh --group-directories-first --time-style=long-iso'

# Simple listing without hidden files
alias ll='ls -C1F'

# Find PID process number by specifying a process name (or grep regexp) as argument
alias pid='ps -acx -o cmd,pid,%cpu,%mem --sort -%mem | grep '

# Mapping to (n)ext/(p)revious when using ctrl+r backward search
bindkey -M isearch '^p' history-incremental-search-backward
bindkey -M isearch '^n' history-incremental-search-forward


## Typos
alias cd..='cd ..'
alias gti='git'


## Docker (in case docker system|image| prune is not efficient
alias docker_clean_all='docker rmi -f $(docker images -a|grep "<none>"|awk '"'"'$1=="<none>" {print $3}'"'"')'


## Virtualenv
export WORKON_HOME=~/.envs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source ~/.local/bin/virtualenvwrapper.sh
alias venv='. ./.env/bin/activate'