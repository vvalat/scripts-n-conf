#!/bin/bash
# This script retrieves all sessions name, providing they follow the same naming convention
# and displays them in the same terminal in different tabs so to check before killing tmux.
# This can be changed to your convention     V
SESSIONS=$(tmux list-sessions | grep -o "SESSION[0-9]*")
ARRAY=$(echo $SESSIONS | tr " " "\n")
COMMAND="gnome-terminal"
if [[ -z $TMUX ]]; then
    for em in ${ARRAY}
    do
        APPEND=" --tab -e 'bash -c \"tmux attach-session -t $em\";bash'"
        COMMAND=${COMMAND}${APPEND}
    done
    eval ${COMMAND}
fi
