#!/bin/bash

SOURCE=$HOME/Pictures/Wallpapers
REFERENCES=$(find $SOURCE -type f -name "*.jpg" -o -name "*.JPG" -o -name "*.png" -o -name "*.PNG" | shuf -n1)
gsettings set org.gnome.desktop.background picture-uri "file://$REFERENCES"
gsettings set org.gnome.desktop.background picture-options "spanned"  # useful for multiple displays
