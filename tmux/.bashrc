# Add this line to your shell config file to start tmux with a new terminal
# You can choose your sessions' naming convention here    V
[[ -z "$TMUX" ]] && bash /absolute/path/to/run-tmux.sh SESSION

# On a side note, it could be better to map this shortcut to a key combo:
# gnome-terminal -e 'bash /absolute/path/to/run-tmux.sh SESSION'
# It could make a difference if the TERM environment variable is not
# properly loaded in the called script
