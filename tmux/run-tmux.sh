#!/bin/bash
# This script sets the TERM environment variable with a color compatible value
# then launch a new session
export TERM=screen-256color
COUNT=$(tmux list-sessions | wc -l)
COUNT=$((COUNT + 1))
if [ $COUNT -eq 0 ]; then
    tmux new-session -s ${1}1
else
    tmux new-session -s ${1}${COUNT}
fi
