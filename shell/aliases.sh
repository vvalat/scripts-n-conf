# Those aliases are to be added in your shell configuration file

# If colours are supported, add them to greps and listings
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls –color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# List all except (./..) with relative size units and some others parameters
alias la='ls -Alh --group-directories-first --time-style=long-iso'
# Simple listing without hidden files
alias ll='ls -C1F'

# Navigation aliases
alias ..='cd ..'
alias .2='cd ../../'
alias .3='cd ../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../../'
# Prevent typos
alias cd..='cd..'

# Find PID process number by specifying a process name (or grep regexp) as argument
alias pid='ps -acx -o cmd,pid,%cpu,%mem --sort -%mem | grep '

# Fast access from command line
alias reboot='sudo /sbin/reboot'
alias poweroff='sudo /sbin/poweroff'
