# Stored in .oh-my-zsh/themes
# Allows modifying prompt on certain conditions bash-style

local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
function machine_status() {
if [[ "$DOCKER_MACHINE_NAME" != "" ]]; then
    echo "/!\ On docker-machine: $DOCKER_MACHINE_NAME /!\\ "
fi
}
PROMPT='${ret_status} %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)%{$fg_bold[white]%}$(machine_status)%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"