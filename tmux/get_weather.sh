#!/bin/bash
# Obviously the URL can be adapted to your needs       V
string=$(wget -q -O- "http://www.accuweather.com/en/fr/paris/623/weather-forecast/623" | \
         awk -F\' '/acm_RecentLocationsCarousel\.push/{print $1": "$13", "$10"°" }'| \
         sed 's/acm_RecentLocationsCarousel\.push({name:"//' | \
         sed 's/", daypart:: , / \//' | \
         sed 's/ \//:/' | \
         sed 's/text:"//' | \
         sed 's/"});//' | \
         head -1)
echo $string
