#!/bin/bash

# Rationale: Sometimes, a small performance boost is needed

CONTAINERS=$(docker ps -q --filter "status=running")
if [[ $CONTAINERS -eq 0 ]]; then
    echo "No containers to remove"
else
    docker kill $CONTAINERS
fi
