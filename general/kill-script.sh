#!/bin/bash
# This script looks for a specific process (here screen) and kills it
# It really could be better but it does the job
PID=$(ps aux c | grep screen | cut -d ' ' -f 4)
if [ "$PID" -eq "$PID" ] 2>/dev/null
then
    kill $PID
else
    PID=$(ps aux c | grep screen | cut -d ' ' -f 5)
    if [ "$PID" -eq "$PID" ] 2>/dev/null
    then
        kill $PID
    else
        PID=$(ps aux c | grep screen | cut -d ' ' -f 6)
        if [ "$PID" -eq "$PID" ] 2>/dev/null
        then
            kill $PID
        else
            echo "No screen process found..."
        fi
    fi
fi
