#! /bin/bash

# Rationale: using a debugger inside a docker container is sometimes a mess considering the input/output capture.
# This should make it easier.

docker-compose stop

# The following should not be necessary outside of a specific context but I leave it here for personal convenience.
# sudo find -type f -name '*.pyc' -delete

# The debug compose file should have specific settings ("tty: true" and "stdin_open: true")
docker-compose -f docker-compose.debug.yml up --build -d

# I could make something up to enter some string to avoid harcoding the container name but I didn't neeed it yet, so this will have to wait.
docker attach $(docker ps -f name=api_api_1 -q) --detach-keys "ctrl-c"