#!/bin/bash

# Rationale: docker uses the main disk to store everything.
# Unless pruning every once in a while, a small disk (SSD for instance) will be filled in no time.
# Hence this script that allows changing the default installation folder to somewhere else (here a .docker directory in the home of 'user').
# Change as necessary (or use env vars).
# Note: docker package update will reset this. If you see a run/up command making some build steps, consider running this again.

# Sudoing is not great but I didn't try with fakeroot
sudo cp /lib/systemd/system/docker.service /lib/systemd/system/docker.service.bak
echo FROM
sed "1s/dockerd -H fd:\/\//dockerd -g \/home\/user\/\.docker\/cache -H fd:\/\//" /lib/systemd/system/docker.service.bak | grep dockerd
echo TO
sed "s/dockerd -H fd:\/\//dockerd -g \/home\/user\/\.docker\/cache -H fd:\/\//" /lib/systemd/system/docker.service.bak | grep dockerd
read -p $'The above content is going to be modified. Do you agree? (y)\n' agree
if [[ "$agree" == "y" ]];then
    sudo sed -i "s/dockerd -H fd:\/\//dockerd -g \/home\/user\/\.docker\/cache -H fd:\/\//" /lib/systemd/system/docker.service
    sudo systemctl daemon-reload
    sudo systemctl stop docker
    sudo systemctl daemon-reload
    sudo systemctl start docker
else
    echo exited
fi