" Vundle is required in this config, get it here:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
" *vim-gui-common* and *vim-runtime* packages are also needed

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SECTION 1 - PLUGINS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

" Inception!
Plugin 'VundleVim/Vundle.vim'

" List the plugins here
Plugin 'vim-airline/vim-airline'                 " Revamped status bar
Plugin 'vim-airline/vim-airline-themes'          " Themes for Airline
Plugin 'chrisbra/Recover.vim'                    " Add a diff option when a swap file is found
Plugin 'scrooloose/nerdtree'                     " Navigation tree
Plugin 'scrooloose/nerdcommenter'                " Key bindings to comment lines/blocks
Plugin 'tpope/vim-fugitive'                      " Git integration
Plugin 'scrooloose/syntastic'                    " Syntax Checker
Plugin 'klen/python-mode'                        " Python IDE
Plugin 'davidhalter/jedi-vim'                    " Python documentation and tools
Plugin 'jmcantrell/vim-virtualenv'               " Virtual environments support
Plugin 'gregsexton/matchtag'                     " Highlights html and xml matched tags
Plugin 'StanAngeloff/php.vim'                    " Improved PHP syntax highlighting
Plugin 'shawncplus/phpcomplete.vim'              " PHP omnicompletion (goto ref require ctags)
Plugin '2072/php-indenting-for-vim'              " Better indenting even when using html in php
Plugin 'pangloss/vim-javascript'                 " Improved JavaScript syntax and indentation
Plugin 'othree/javascript-libraries-syntax.vim'  " Extended JS syntax libraries
Plugin 'ternjs/tern_for_vim'                     " JavaScript autocompletion (requires node and specific npm install)
Plugin 'elzr/vim-json'                           " JSON syntax, folding

call vundle#end()
filetype plugin indent on


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SECTION 2 - INTERFACE
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Airline Setup
set t_Co=256
set laststatus=2

" Refresh airline on mode change (Jedi display bug)
autocmd InsertLeave * AirlineRefresh

" For theme preview/setting, use :AirlineTheme <name>
let g:airline_theme = 'onedark'

let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" Unicode symbols (for compatibility use)
"let g:airline_left_sep = '»'
"let g:airline_left_sep = '▶'
"let g:airline_right_sep = '◀'
"let g:airline_right_sep = '«'
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '␤'
"let g:airline_symbols.linenr = '¶'
"let g:airline_symbols.branch = '⎇'
"let g:airline_symbols.paste = 'ρ'
"let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.paste = '∥'
"let g:airline_symbols.whitespace = 'Ξ'

" Airline symbols
" If need be, download patched fonts here: https://github.com/powerline/fonts
" Those are to be defined as the terminal font and should appear correctly below:
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" For a list of flags to display -> Airline doc | :help statusline
function! AirlineInit()
    let g:airline_section_a = airline#section#create(['mode',' ','branch'])
    let g:airline_section_b = airline#section#create(['%f',' ','readonly'])
    let g:airline_section_c = airline#section#create(['filetype'])
    let g:airline_section_x = airline#section#create(['%P'])
    let g:airline_section_y = airline#section#create(['%B'])
    let g:airline_section_z = airline#section#create(['l:','%l',' c:','%c'])
endfunction
autocmd VimEnter * call AirlineInit()
autocmd VimEnter * :AirlineRefresh

" Undo Directory
if !isdirectory($HOME."/.vim")
    call mkdir($HOME."/.vim", "", 0770)
endif
if !isdirectory($HOME."/.vim/undo-dir")
    call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif
set undodir=~/.vim/undo-dir
set undofile


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SECTION 3 - PRODUCTIVITY HACKS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Ignore compiled, temporary and minified files when browsing directories
set wildignore=*.o,*~,*.pyc,*.swp,*.min.*

" Tabulation/Indentation (disregard modelines, replace tab/indent by 4 spaces...)
set modelines=0
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent

" Search options (Smart case applies sensitivity for things like camel case)
set ignorecase
set smartcase

" Display line numbers and use <leader>l to toggle them
set number
map <leader>l :set number!<CR>

" Replace matching parenthesis color
hi MatchParen cterm=NONE ctermbg=grey ctermfg=white

" Include special characters as part of names (PHP/Bash vars, Python decorators...)
set iskeyword+=$,@

" Backspace will remove anything (line breaks, auto indentation...)
set backspace=indent,eol,start

" Disable auto-inserting comment character after line break (return)
autocmd Filetype * set formatoptions-=r


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SECTION 4 - VISUALS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Color schemes are to be installed in .vim/colors
colorscheme spring-night

" Let there be colors
syntax on

" Amount of lines above and below cursor when scrolling
set scrolloff=4

" Do not repeat mode on last line
set noshowmode

" Discreet line number highlight
hi clear CursorLine
hi CursorLineNR cterm=bold
set cursorline

" <Tab> show a full list of command on a single line
set wildmenu
set wildmode=full

" No beeping
set visualbell

" Smooth I/O with terminal
set ttyfast

" Display tabs and spaces
set list listchars=trail:.,tab:>-
" Another way if the previous fails
"autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkgrey
"autocmd BufWinEnter * match ExtraWhitespace /\s\+$/


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SECTION 5 - MAPPINGS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Allows jumping to next display line when scrolling with h,j,k,l


" The behavior of up,down,left,right is still relative to physical lines
nnoremap j gj
nnoremap k gk
" Below are hacks that substitute escape codes for the commands in tmux context

" Use <ctrl><left,right> to navigate tabs and <shift-ctlr><left,right> to move them
" Use <ctrl><up,down> to move one or more lines


" Tmux conf file should have this value: 'set-window-option -g xterm-keys on'
" In order to check if the codes are correct, use 'cat' in shell for '^[' codes
if $TERM =~ 'screen'
    nnoremap <ESC>[1;5D :tabprevious<CR>
    nnoremap <ESC>[1;5C :tabnext<CR>
    nnoremap <silent> <ESC>[1;6D :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
    nnoremap <silent> <ESC>[1;6C :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>
    nnoremap <silent> <ESC>[1;5B :m+<CR>==
    nnoremap <silent> <ESC>[1;5A :m-2<CR>==
    inoremap <silent> <ESC>[1;5B <Esc>:m+<CR>==gi
    inoremap <silent> <ESC>[1;5A <Esc>:m-2<CR>==gi
    vnoremap <silent> <ESC>[1;5B :m '>+1<CR>gv== gv
    vnoremap <silent> <ESC>[1;5A :m '<-2<CR>gv== gv
else
    nnoremap <C-Left> :tabprevious<CR>
    nnoremap <C-Right> :tabnext<CR>
    nnoremap <silent> <C-S-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
    nnoremap <silent> <C-S-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>
    nnoremap <silent> <C-Down> :m+<CR>==
    nnoremap <silent> <C-Up> :m-2<CR>==
    inoremap <silent> <C-Down> <Esc>:m+<CR>==gi
    inoremap <silent> <C-Up> <Esc>:m-2<CR>==gi
    vnoremap <silent> <C-Down> :m '>+1<CR>gv== gv
    vnoremap <silent> <C-Up> :m '<-2<CR>gv== gv
endif

" Nerd tree
nnoremap <F2> :NERDTreeToggle<CR>

" Use 0 to get to first non-blank character (go to beginning of indented line)
nnoremap 0 ^


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SECTION 6 - LANGUAGES SPECIFICS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Global settings
"****************************

" Change tab/indent to 2 spaces for specific files (js are handled by vim-javascript)
function! ChangeIndentation()
    for type in ['html', 'css', 'xml']
        if &filetype == type
            set tabstop=2
            set shiftwidth=2
            set softtabstop=2
        endif
    endfor
endfunction
autocmd VimEnter * call ChangeIndentation()

" Set basic wrapping settings
set wrap
set textwidth=100

" Systastic settings
let g:syntastic_aggregate_errors = 1

" ! Specific checkers require package installation !


" Python
"****************************

" Highlighting characters past column 100
augroup python_visual_overflow
    " Clearing previous commands
    autocmd!
    autocmd Filetype python set colorcolumn=100
    autocmd Filetype python highlight ColorColumn ctermbg=131
    autocmd FileType python highlight Excess ctermbg=124
    autocmd FileType python match Excess /\%101v.*/
    autocmd FileType python set nowrap
augroup END

" PyMode settings
let g:pymode_options_max_line_length = 100
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_run = 1          " Run code in buffer/selection with <leader>r
let g:pymode_breakpoint = 1   " Set breakpoint with <leader>b
let bp = 'import sys; import pdb; pdb.Pdb(stdout=sys.__stdout__).set_trace() # XXX breakpoint'
let g:pymode_breakpoint_cmd = g:bp
let g:pymode_virtualenv = 1

" Folding and unfolding
let g:pymode_folding = 1
autocmd Filetype python set foldlevel=100
function! ToggleFolding()
    if &foldlevel == 100
        set foldlevel=0
    else
        set foldlevel=100
    endif
endfunction
autocmd Filetype python map ,f :<C-U>call ToggleFolding()<CR>

" Delegate lint to Syntastic and navigation to Jedi
let g:pymode_lint = 0
let g:pymode_rope = 0

" Checkers may include: pylint, python (default checker based on pyflakes)
let g:syntastic_python_checkers = ["pyflakes", "flake8", "pylint"]

" Ignore 79 chars limit in PEP8
let g:syntastic_python_flake8_post_args = '--ignore=E501'

" Show lint error code for pylint
let msgtpl = '--msg-template="{path}:{line}:{column}:{C}: [{msg_id}] {msg}"'
let g:syntastic_python_pylint_post_args = msgtpl

" Prevent Jedi from omnicompleting on accessing attributes,methods or modules
let g:jedi#popup_on_dot = 0
let g:jedi#smart_auto_mappings = 0

" Display help on arguments in command line instead of buffer view (1)
let g:jedi#show_call_signatures = 2
let g:jedi#show_call_signatures_delay = 0

" Limit split height for documention
let g:jedi#max_doc_height = 10


" PHP
"****************************

" Remap omnicompletion to Jedi standard
inoremap <C-@> <C-x><C-o>

" Adapt prettified multipart php standard to standard (compliant with php.vim)
let g:NERDCustomDelimiters = { 'php': { 'left': '//', 'leftAlt': '/**', 'rightAlt': '*/' } }

"Customization example (allow highlight in comments) for php.vim plugin
function! PhpSyntaxOverride()
    hi! def link phpDocTags  phpDefine
    hi! def link phpDocParam phpType
endfunction

augroup phpSyntaxOverride
    autocmd!
    autocmd FileType php call PhpSyntaxOverride()
augroup END


" JSON
"****************************

" JSON Folding
autocmd Filetype json set foldmethod=syntax
autocmd Filetype json set foldlevel=100
autocmd Filetype json map ,f :<C-U>call ToggleFolding()<CR>
