"""
Methods relating to data structures (creation, modification, extraction...)
"""
from typing import List


def split_list_by_target_size(input_list: List, size=1) -> List[List]:
    """
    Small utility function to split a list into smaller ones defined by their maximum size
    >>> split_list_by_target_size([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], max_count=3)
    [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11]]
    """
    output_list = []
    for upper_bound in list(range(size - 1, len(input_list), size)):
        output_list.append(input_list[(upper_bound - size + 1):(upper_bound + 1)])
    if overflow := (len(input_list) % size):
        output_list.append(input_list[(-overflow):])
    return output_list
