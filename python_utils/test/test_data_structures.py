from ..data_structures import split_list_by_target_size


def test_split_list_by_target_size():
    # Basic verification of list count
    assert len(split_list_by_target_size(list(range(0, 10)))) == 10
    assert len(split_list_by_target_size(list(range(0, 10)), size=2)) == 5
    assert len(split_list_by_target_size(list(range(0, 10)), size=3)) == 4
    assert len(split_list_by_target_size(list(range(0, 10)), size=3)[0]) == 3

    # Overflow check
    assert len(split_list_by_target_size(list(range(0, 10)), size=3)[-1]) == 1
    assert "Austin" in split_list_by_target_size(["New York", "Chicago", "Austin"], size=2)[-1]

    # Output check
    assert split_list_by_target_size(["oranges", "milk", "bread", "jam"]) == [
        ["oranges"], ["milk"], ["bread"], ["jam"]
    ]
    assert split_list_by_target_size(
        ["Alice", "Bob", "Charles", "Dean", "Evelyn", "Francis", "Gene", "Hector", "Irene"]
        , size=4
    ) == [["Alice", "Bob", "Charles", "Dean"], ["Evelyn", "Francis", "Gene", "Hector"], ["Irene"]]
