# This script forces virtualenv prompt to conform to the usual prompt
# The line below should be added to a 'postactivate' file in .venvs/ or.virtualenvs
# The prompt configuration for the virtualenv part is in the 'activate' script
PS1="\[\e[1;33;45m\] (`basename \"$VIRTUAL_ENV\"`) \[\e[0m\]$_OLD_VIRTUAL_PS1"
